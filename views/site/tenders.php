<?php

use yii\grid\GridView;

/** @var yii\data\ActiveDataProvider $listDataProvider */

echo GridView::widget(
    [
        'dataProvider' => $listDataProvider,
        'columns' => [
            'id',
            'tenderID',
            'description',
            'valueAmount',
            'dateModified'
        ]
    ]
);