<?php

declare(strict_types=1);

namespace app\services\prozorro;

use app\adapters\YiiLoggerAdapter;
use app\services\prozorro\dtos\TenderDetailsDTO;
use app\services\prozorro\dtos\TendersDTO;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;


class ProzorroApiService
{

    const URI = 'https://public.api.openprocurement.org/';

    const ENDPOINT = 'api/0/tenders';

    const MAX_LIMIT = 1000;

    public function __construct(
        protected ClientInterface $client = new Client(['base_uri' => self::URI]),
        protected LoggerInterface $logger = new YiiLoggerAdapter(),
    ) {
    }

    public function getTenders(
        int $descending = 1,
        int $limit = 100,
        ?string $offset = null,
        ...$otherParameters
    ): TendersDTO {
        $limit = $limit > self::MAX_LIMIT ? self::MAX_LIMIT : $limit;
        $queryParameters = [...compact('descending', 'limit', 'offset'), ...$otherParameters];
        $response = $this->doRequest(
            uriEndpoint: static::ENDPOINT . '?' . http_build_query($queryParameters)
        );

        return new TendersDTO($this->getResponseContent($response));
    }

    public function getTenderDetails(
        string $id
    ): TenderDetailsDTO {
        $response = $this->doRequest(
            uriEndpoint: static::ENDPOINT . '/' . $id
        );

        return new TenderDetailsDTO($this->getResponseContent($response));
    }

    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = 'GET'
    ): Response {
        try {
            $response = $this->client->request(
                $requestMethod,
                $uriEndpoint,
                $params
            );
        } catch (GuzzleException $exception) {
            $response = new Response(status: $exception->getCode(), body: $exception->getMessage());
        }

        return $response;
    }

    private function getResponseContent(
        Response $response
    ): array {
        $this->catchError($response);

        return json_decode($response->getBody()->getContents(), true);
    }

    private function catchError(
        Response $response
    ): void {
        $status = $response->getStatusCode();

        if ($status >= 400) {
            $responseContent = $response->getBody()->getContents();
            $this->logger->log(\Psr\Log\LogLevel::ERROR, $responseContent);
            throw new \Exception($responseContent, $status);
        }
    }
}
