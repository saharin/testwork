<?php


namespace app\services\prozorro\dtos;


use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\Casters\DataTransferObjectCaster;
use Spatie\DataTransferObject\DataTransferObject;

class TenderDTO extends DataTransferObject
{
    public string $id;
    public string $dateModified;

}