<?php


namespace app\services\prozorro\dtos;


use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\DataTransferObject;

class TenderDetailsDTO extends DataTransferObject
{

    #[MapFrom('data.id')]
    public string $id;

    #[MapFrom('data.dateModified')]
    public string $dateModified;

    #[MapFrom('data.tenderID')]
    public null|string $tenderID;

    #[MapFrom('data.description')]
    public null|string $description;

    #[MapFrom('data.title')]
    public null|string $title;

    #[MapFrom('data.value.amount')]
    public null|float $valueAmount;

}