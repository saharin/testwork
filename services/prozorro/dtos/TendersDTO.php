<?php


namespace app\services\prozorro\dtos;


use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\Casters\DataTransferObjectCaster;
use Spatie\DataTransferObject\DataTransferObject;

class TendersDTO extends DataTransferObject
{
    //#[CastWith(DataTransferObjectCaster::class, TenderDetailsDTO::class)]
    #[CastWith(ArrayCaster::class, itemType: TenderDTO::class)]
    public array $data;

    #[MapFrom('next_page.offset')]
    public float $offset;

}