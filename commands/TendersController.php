<?php


namespace app\commands;

use app\models\Tender;
use app\services\prozorro\ProzorroApiService;
use app\services\ProzorroTenders;
use app\services\TenderDTO;
use GuzzleHttp\Client;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception;
use yii\helpers\Console;

class TendersController extends Controller
{

    public int $quantity = 100;

    protected ProzorroApiService $prozorroApiService;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->prozorroApiService = new ProzorroApiService();
    }

    public function options($actionID)
    {
        return ['quantity'];
    }

    public function optionAliases()
    {
        return ['q' => 'quantity'];
    }

    public function actionUpdate()
    {
        $offset = null;
        $page = 1;

        if ($this->quantity > ProzorroApiService::MAX_LIMIT) {
            $page = intdiv($this->quantity, ProzorroApiService::MAX_LIMIT);
            if ($this->quantity % ProzorroApiService::MAX_LIMIT > 0) {
                $page++;
            }
        }

        while ($page-- > 0) {
            try {
                $tendersDTO = $this->prozorroApiService->getTenders(limit: $this->quantity, offset: $offset);

                foreach ($tendersDTO->data as $tenderDTO) {
                    if (!$tender = Tender::findOne(['id' => $tenderDTO->id])) {
                        $tender = new Tender();
                    }

                    if ($tender->dateModified === $tenderDTO->dateModified) {
                        continue;
                    }

                    try {
                        $tenderDetailsDTO = $this->prozorroApiService->getTenderDetails($tenderDTO->id);
                        $tender->setAttributes($tenderDetailsDTO->toArray());
                        $tender->description = empty($tenderDetailsDTO->description) ? $tenderDetailsDTO->title : $tenderDetailsDTO->description;
                        $tender->save();
                    } catch (\Exception $e) {
                        echo "$tenderDTO->id save fail\n";
                    }
                }
            } catch (\Exception $e) {
                return ExitCode::UNSPECIFIED_ERROR;
            }

            $offset = $tendersDTO->offset;
            $this->quantity -= count($tendersDTO->data);
        }

        return ExitCode::OK;
    }

}