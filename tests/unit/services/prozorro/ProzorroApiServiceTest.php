<?php

namespace tests\unit\services\prozorro;

use app\services\prozorro\dtos\TenderDetailsDTO;
use app\services\prozorro\dtos\TendersDTO;
use app\services\prozorro\ProzorroApiService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\IsType;

class ProzorroApiServiceTest extends \Codeception\Test\Unit
{

    public function testGetTendersReturnTendersDTO()
    {
        $validResponseJsonSchema = json_encode(
            [
                'data' => [
                    [
                        'dateModified' => '2022-09-16T11:22:57.286478+03:00',
                        'id' => '73971e15daa04b1fa21dc8d32c9ae1ed'
                    ]
                ],
                'next_page' => [
                    'offset' => 1663316577.289
                ]
            ]
        );

        $prozorroApiService = new ProzorroApiService(
            $this->getMockHttpClient($validResponseJsonSchema)
        );

        verify(get_class($prozorroApiService->getTenders()))->equals(TendersDTO::class);
    }

    public function testGetTenderByIdReturnTenderDetailsDTO()
    {
        $validResponseJsonSchema = json_encode(
            [
                'data' => [
                    'dateModified' => '2022-09-16T11:22:57.286478+03:00',
                    'id' => '73971e15daa04b1fa21dc8d32c9ae1ed'
                ]
            ]
        );

        $prozorroApiService = new ProzorroApiService(
            $this->getMockHttpClient($validResponseJsonSchema)
        );

        verify(get_class($prozorroApiService->getTenderDetails('')))->equals(TenderDetailsDTO::class);
    }

    public function testGetTendersBadResponseExceptionThrown()
    {
        try {
            (new ProzorroApiService($this->getMockHttpClient(status: 400)))->getTenders();
        } catch (\Exception $e) {
            return true;
        }
        return false;
    }

    public function testGetTenderDetailsBadResponseExceptionThrown()
    {
        try {
            (new ProzorroApiService($this->getMockHttpClient(status: 500)))->getTenderDetails('');
        } catch (\Exception $e) {
            return true;
        }
        return false;
    }

    protected function getMockHttpClient(string $body = null, int $status = 200)
    {
        return new Client(
            [
                'handler' => HandlerStack::create(
                    new MockHandler(
                        [
                            new Response(status: $status, body: $body),
                        ]
                    )
                )
            ]
        );
    }

}