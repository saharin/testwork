<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenders".
 *
 * @property int $pk
 * @property string $id
 * @property string|null $tenderID
 * @property string|null $description
 * @property float|null $valueAmount
 * @property string|null $dateModified
 */
class Tender extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['description'], 'string'],
            [['valueAmount'], 'number'],
            [['id', 'tenderID', 'dateModified'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'id' => 'ID',
            'tenderID' => 'Tender ID',
            'description' => 'Description',
            'valueAmount' => 'Value Amount',
            'dateModified' => 'Date Modified',
        ];
    }
}
