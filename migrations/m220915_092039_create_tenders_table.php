<?php

use yii\db\Migration;

/**
 * Class m220915_092039_tenders
 */
class m220915_092039_create_tenders_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable(
            'tenders',
            [
                'pk' => $this->primaryKey(),
                'id' => $this->string()->notNull()->unique(),
                'tenderID' => $this->string(),
                'description' => $this->text(),
                'valueAmount' => $this->float()->unsigned(),
                'dateModified' => $this->string(),
            ]
        );
    }

    public function down()
    {
        $this->dropTable('tenders');
    }

}
