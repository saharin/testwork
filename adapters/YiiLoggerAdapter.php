<?php

namespace app\adapters;

use Psr\Log\LoggerInterface;

class YiiLoggerAdapter implements LoggerInterface
{

    use \Psr\Log\LoggerTrait;

    protected $adaptation;

    public function __construct()
    {
        $this->adaptation = \Yii::getLogger();
    }

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        $this->adaptation->log($message, $level);
    }
}